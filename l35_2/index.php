<!DOCTYPE html>
<html lang="en">
  <head>
    <title></title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="css/style.css" rel="stylesheet">
  </head>
  <body>
<?php
    ini_set('display_errors', 1);
    ini_set('log_errors', 1);
    ini_set('error_log', dirname('__FILE__') . "/log.txt");
    error_reporting(E_ALL);

    $servername="127.0.0.1"; 
    $username="antasa"; 
    $password="passwd"; 
    $dbname="store"; 

    // Create tables if they don't exist
    $conn = new mysqli($servername, $username, $password, $dbname);

    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $sql = "CREATE TABLE IF NOT EXISTS
  disciplines(
    discipline_code SMALLINT UNSIGNED NOT NULL PRIMARY KEY,
    discipline_name VARCHAR(100) NOT NULL,
    specialty VARCHAR(100) NOT NULL,
    course INT UNSIGNED NOT NULL
  ) ENGINE = InnoDB;";

    if ($conn->query($sql) === TRUE) {
        echo "Table personal_file created successfully";
    } else {
        echo "Error creating table: " . $conn->error;
    }

    $sql = "CREATE TABLE IF NOT EXISTS
  types_of_load (
    class_type VARCHAR(100) NOT NULL PRIMARY KEY,
    name_of_the_load VARCHAR(100) NOT NULL
  ) ENGINE = InnoDB;";

    if ($conn->query($sql) === TRUE) {
        echo "Table tariff_grid created successfully";
    } else {
        echo "Error creating table: " . $conn->error;
    }

    $sql = "CREATE TABLE IF NOT EXISTS
  load_execution(
    id INT UNSIGNED PRIMARY KEY,
    date DATE,
    pair_number INT,
    group_number INT,
    discipline_code INT,
    topic_title VARCHAR(100) NOT NULL,
    class_type INT,
    amount_hours INT
  ) ENGINE = InnoDB;";

    if ($conn->query($sql) === TRUE) {
        echo "Table timesheet created successfully";
    } else {
        echo "Error creating table: " . $conn->error;
    }

    // Fill
    // Fill disciplines table with data
//     $sql1 = "INSERT INTO disciplines (discipline_code, discipline_name, specialty, course)
//         VALUES
//         (001, 'Mathematics', 'Math', 1),
//         (002, 'Computer Science', 'IT', 2),
//         (003, 'Physics', 'Science', 2),
//         (004, 'History', 'Social Science', 3)";
//
// mysqli_query($conn, $sql1);
//
// // Fill types_of_load table with data
// $sql2 = "INSERT INTO types_of_load (class_type, name_of_the_load)
//         VALUES
//         ('Lecture', 'Theoretical instruction'),
//         ('Lab', 'Experimental instruction'),
//         ('Seminar', 'Group instruction')";
//
// mysqli_query($conn, $sql2);
//
// Fill load_execution table with data
// $sql3 = "INSERT INTO load_execution (id, date, pair_number, group_number, discipline_code, topic_title, class_type, amount_hours)
//         VALUES
//         (001, '2021-09-01', 1, 101, 001, 'Introduction to Calculus', '1', 2),
//         (002, '2021-09-02', 2, 102, 002, 'Data Structures and Algorithms', '1', 2),
//         (003, '2021-09-03', 3, 103, 003, 'Classical Mechanics', '2', 3),
//         (004, '2021-09-04', 4, 104, 004, 'World History', '2', 2),
//         (005, NULL, 4, 104, 004, 'World History', '2', 2)";
// 
// mysqli_query($conn, $sql3);



    // Count number of records in personal_file table
    $sql = "SELECT COUNT(*) as count FROM disciplines;";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
        echo "Total number of records in personal_file table: " . $row["count"];
    } else {
        echo "0 results";
    }

    // Search for records that start with user input letter
    if(isset($_POST['submit']) && !empty($_POST['letter'])) {
        $letter = $_POST['letter'];
        $sql = "SELECT * FROM disciplines WHERE discipline_name LIKE '$letter%';";
        $result = $conn->query($sql);

        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                echo "id: " . $row["id"]. " - Name: " . $row["first_name"]. " " . $row["last_name"]. "<br>";
            }
        } else {
            echo "0 results";
        }
    }

    // Подсчитать сумму для одного из числовых полей.
    $sql = "SELECT SUM(amount_hours) as total_working_hours FROM load_execution;";
    $result = $conn->query($sql);
    $row = mysqli_fetch_assoc($result);
    echo "Total working hours: " . $row['total_working_hours'];

    echo "<br>";

    // Показать пять записей, имеющих наибольшее значение.
    $sql = "SELECT * FROM load_execution ORDER BY amount_hours DESC LIMIT 5; ";
    $result = $conn->query($sql);
    while ($row = mysqli_fetch_assoc($result)) {
        echo "id" . $row['id'] . ": " . $row['amount_hours'] . " hours" . "<br>";
    }

    echo "<br>";

    //Показать записи, в которых присутствует значение, введенное в качестве параметра.
    $search_term = 'developer';
    $sql = "SELECT * FROM disciplines WHERE discipline_name LIKE '%$search_term%'; ";
    $result = $conn->query($sql);
    while ($row = mysqli_fetch_assoc($result)) {
        echo $row['last_name'] . ", " . $row['first_name'] . " - " . $row['position'] . "<br>";
    }

    echo "<br>";

    //Показать запись, содержащую максимальное значение.
    $sql = "SELECT * FROM load_execution WHERE amount_hours = (SELECT MAX(amount_hours) FROM load_execution) LIMIT 1;";
    $result = $conn->query($sql);
    $row = mysqli_fetch_assoc($result);
    echo "load " . $row['id'] . " has the maximum working hours: " . $row['amount_hours'];

    echo "<br>";
    echo "<br>";

    // Для показа 3 записей, содержащих максимальные значения, можно использовать следующий SQL-запрос:
    $sql = "SELECT * FROM load_execution ORDER BY amount_hours DESC LIMIT 3;";
    $result = $conn->query($sql);
    while ($row = mysqli_fetch_assoc($result)) {
        echo "ID: " . $row['id'] . "<br>";
        echo "Date: " . $row['date'] . "<br>";
        echo "pair_number: " . $row['pair_number'] . "<br>";
        echo "group_number: " . $row['group_number'] . "<br>";
        echo "discipline_code: " . $row['discipline_code'] . "<br>";
        echo "topic_title: " . $row['topic_title'] . "<br>";
        echo "class_type: " . $row['class_type'] . "<br>";
        echo "amount_hours: " . $row['amount_hours'] . "<br><br>";
    }

    $sql = "SELECT * FROM load_execution WHERE date IS NULL;";
    $result = $conn->query($sql);
    while ($row = mysqli_fetch_assoc($result)) {
        echo "ID: " . $row['id'] . "<br>";
        echo "Date: " . $row['date'] . "<br>";
        echo "pair_number: " . $row['pair_number'] . "<br>";
        echo "group_number: " . $row['group_number'] . "<br>";
        echo "discipline_code: " . $row['discipline_code'] . "<br>";
        echo "topic_title: " . $row['topic_title'] . "<br>";
        echo "class_type: " . $row['class_type'] . "<br>";
        echo "amount_hours: " . $row['amount_hours'] . "<br><br>";
    }

    $conn->close();
?>
  </body>
</html>
