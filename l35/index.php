<!DOCTYPE html>
<html lang="en">
  <head>
    <title></title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="css/style.css" rel="stylesheet">
  </head>
  <body>
<?php
    ini_set('display_errors', 1);
    ini_set('log_errors', 1);
    ini_set('error_log', dirname('__FILE__') . "/log.txt");
    error_reporting(E_ALL);

    $servername="127.0.0.1"; 
    $username="antasa"; 
    $password="passwd"; 
    $dbname="store"; 

    // Create tables if they don't exist
    $conn = new mysqli($servername, $username, $password, $dbname);

    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $sql = "CREATE TABLE IF NOT EXISTS personal_file (
       id INT(11) NOT NULL AUTO_INCREMENT,
       last_name VARCHAR(50) NOT NULL,
       first_name VARCHAR(50) NOT NULL,
       birth_date DATE NOT NULL,
       address VARCHAR(100) NOT NULL,
       position VARCHAR(50) NOT NULL,
       PRIMARY KEY (id)
    )";

    if ($conn->query($sql) === TRUE) {
        echo "Table personal_file created successfully";
    } else {
        echo "Error creating table: " . $conn->error;
    }

    $sql = "CREATE TABLE IF NOT EXISTS tariff_grid (
       id INT(11) NOT NULL AUTO_INCREMENT,
       position VARCHAR(50) NOT NULL,
       tariff FLOAT NOT NULL,
       PRIMARY KEY (id)
    )";

    if ($conn->query($sql) === TRUE) {
        echo "Table tariff_grid created successfully";
    } else {
        echo "Error creating table: " . $conn->error;
    }

    $sql = "CREATE TABLE IF NOT EXISTS timesheet (
       id INT(11) NOT NULL AUTO_INCREMENT,
       employee_id INT(11) NOT NULL,
       month INT(2) NOT NULL,
       year INT(4) NOT NULL,
       working_hours INT(3) NOT NULL,
       vacation_days INT(2) NOT NULL,
       sick_leave_days INT(2) NOT NULL,
       PRIMARY KEY (id),
       FOREIGN KEY (employee_id) REFERENCES personal_file(id)
    )";

    if ($conn->query($sql) === TRUE) {
        echo "Table timesheet created successfully";
    } else {
        echo "Error creating table: " . $conn->error;
    }

    // Fill
    // $sql = "INSERT INTO personal_file (last_name, first_name, birth_date, address, position) 
    // VALUES ('Doe', 'John', '1990-01-01', '123 Main St', 'Manager'),
    //        ('Smith', 'Jane', '1995-05-15', '456 Elm St', 'Salesperson'),
    //        ('Johnson', 'Bob', '1985-10-20', '789 Oak Ave', 'Accountant')";
    //
    // if ($conn->query($sql) === TRUE) {
    //     echo "Records inserted successfully into personal_file";
    // } else {
    //     echo "Error inserting records into personal_file: " . $conn->error;
    // }
    //
    // $sql = "INSERT INTO tariff_grid (position, tariff) 
    // VALUES ('Manager', 100),
    //        ('Salesperson', 50),
    //        ('Accountant', 75)";
    //
    // if ($conn->query($sql) === TRUE) {
    //     echo "Records inserted successfully into tariff_grid";
    // } else {
    //     echo "Error inserting records into tariff_grid: " . $conn->error;
    // }
    //
    // $sql = "INSERT INTO timesheet (employee_id, month, year, working_hours, vacation_days, sick_leave_days) 
    // VALUES (1, 1, 2022, 160, 20, 10),
    //        (2, 1, 2022, 140, 15, 5),
    //        (3, 1, 2022, 170, 25, 15)";
    //
    // if ($conn->query($sql) === TRUE) {
    //     echo "Records inserted successfully into timesheet";
    // } else {
    //     echo "Error inserting records into timesheet: " . $conn->error;
    // }



    // Count number of records in personal_file table
    $sql = "SELECT COUNT(*) as count FROM personal_file";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
        echo "Total number of records in personal_file table: " . $row["count"];
    } else {
        echo "0 results";
    }

    // Search for records that start with user input letter
    if(isset($_POST['submit']) && !empty($_POST['letter'])) {
        $letter = $_POST['letter'];
        $sql = "SELECT * FROM personal_file WHERE last_name LIKE '$letter%'";
        $result = $conn->query($sql);

        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                echo "id: " . $row["id"]. " - Name: " . $row["first_name"]. " " . $row["last_name"]. "<br>";
            }
        } else {
            echo "0 results";
        }
    }

    // Подсчитать сумму для одного из числовых полей.
    $sql = "SELECT SUM(working_hours) as total_working_hours FROM timesheet";
    $result = $conn->query($sql);
    $row = mysqli_fetch_assoc($result);
    echo "Total working hours: " . $row['total_working_hours'];

    // Показать пять записей, имеющих наибольшее значение.
    $sql = "SELECT * FROM timesheet ORDER BY working_hours DESC LIMIT 5";
    $result = $conn->query($sql);
    while ($row = mysqli_fetch_assoc($result)) {
        echo "Employee " . $row['employee_id'] . ": " . $row['working_hours'] . " working hours" . "<br>";
    }

    //Показать записи, в которых присутствует значение, введенное в качестве параметра.
    $search_term = 'developer';
    $sql = "SELECT * FROM personal_file WHERE position LIKE '%$search_term%'";
    $result = $conn->query($sql);
    while ($row = mysqli_fetch_assoc($result)) {
        echo $row['last_name'] . ", " . $row['first_name'] . " - " . $row['position'] . "<br>";
    }

    //Показать запись, содержащую максимальное значение.
    $sql = "SELECT * FROM timesheet ORDER BY working_hours DESC LIMIT 1";
    $result = $conn->query($sql);
    $row = mysqli_fetch_assoc($result);
    echo "Employee " . $row['employee_id'] . " has the maximum working hours: " . $row['working_hours'];

    // Для показа 3 записей, содержащих максимальные значения, можно использовать следующий SQL-запрос:
    $sql = "SELECT * FROM timesheet ORDER BY working_hours DESC LIMIT 3";
    $result = $conn->query($sql);
    while ($row = mysqli_fetch_assoc($result)) {
        echo "Employee ID: " . $row['employee_id'] . "<br>";
        echo "Month: " . $row['month'] . "<br>";
        echo "Year: " . $row['year'] . "<br>";
        echo "Working hours: " . $row['working_hours'] . "<br>";
        echo "Vacation days: " . $row['vacation_days'] . "<br>";
        echo "Sick leave days: " . $row['sick_leave_days'] . "<br><br>";
    }

    $sql = "SELECT * FROM timesheet WHERE working_hours = 0 OR vacation_days = 0 OR sick_leave_days = 0";
    $result = $conn->query($sql);
    while ($row = mysqli_fetch_assoc($result)) {
        echo "Employee ID: " . $row['employee_id'] . "<br>";
        echo "Month: " . $row['month'] . "<br>";
        echo "Year: " . $row['year'] . "<br>";
        echo "Working hours: " . $row['working_hours'] . "<br>";
        echo "Vacation days: " . $row['vacation_days'] . "<br>";
        echo "Sick leave days: " . $row['sick_leave_days'] . "<br><br>";
    }

    $conn->close();
?>
  </body>
</html>
