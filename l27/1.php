<!DOCTYPE html>
<html lang="en">
  <head>
    <title></title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  </head>
  <body>
    <script>
      function set_name() {
        document.cookie = "name=" + prompt();
      }
      function set_age() {
        document.cookie = "age=" + prompt();
      }
      function listCookies() {
          var theCookies = document.cookie.split(';');
          var aString = '';
          for (var i = 1 ; i <= theCookies.length; i++) {
              aString += theCookies[i-1] + "<br>";
          }
          return aString;
      }
      var nAgt = navigator.userAgent;
      // trim the fullVersion string at semicolon/space if present
      document.write(''
        + "JS<br>"
        + listCookies() + "<br>");

    </script>

    <input id="name" type="button" value="change name" onclick="set_name();" />
    <br>
    <input id="age" type="button" value="change age" onclick="set_age();" />
    <br>
    <br>

    <?php
      if (!empty($_GET['act'])) {
        echo "PHP<br>";
        print_r($_COOKIE);
        echo "<br>";
        if (!empty($_COOKIE["name"])){
          echo "name: $_COOKIE[name]";
          echo "<br>";
        } else {
          $name = "noname";
          setcookie("name", $name);
          echo "name: $_COOKIE[name]";
          echo "<br>";
        }
        if (!empty($_COOKIE["age"])){
          echo "age: $_COOKIE[age]";
        } else {
          $age = 0;
          setcookie("age", $age, time() + 3600);  // срок действия - 1 час (3600 секунд)
          echo "age: $_COOKIE[age]";
          echo "<br>";
          echo "Куки установлены";
        }
        echo "<br>";
      } else {
    ?>
    <form action="1.php" method="get">
      <input type="hidden" name="act" value="run">
      <input type="submit" value="Run PHP script!">
    </form>
    <?php
      }
    ?>
  </body>
</html>

