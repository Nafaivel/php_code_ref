<!DOCTYPE html>
<html lang="en">
  <head>
    <title></title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="css/style.css" rel="stylesheet">
  </head>
  <body>
    <?php
      ini_set('display_errors', 1);
      ini_set('log_errors', 1);
      ini_set('error_log', dirname('__FILE__') . "/log.txt");
      error_reporting(E_ALL);

      // соединение с сервером базы данных 
      // задание переменных базы данных 
      $host="127.0.0.1"; 
      $user=""; 
      $password=""; 
      $dbname=""; 

      // установка соединения
      $link = mysqli_connect($host, $user, $password, $dbname);

      // проверка на ошибки
      if (!$link) {
          echo "Не могу соединиться с сервером базы данных<br>";
          exit();
      }

      echo "Соединение с сервером базы данных произошло успешно<br>";

      // создание новой таблицы
      $myquery = "CREATE TABLE IF NOT EXISTS 
          SGroup(
              GrpPK INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
              Num VARCHAR(20),
              DepFK INT NOT NULL,
              Year INT,
              Quantify INT,
              Rating INT
          )ENGINE = InnoDB;";

      // выполнение запроса
      if (!mysqli_query($link, $myquery)) {
          echo "Не могу выполнить запрос<br>";
          exit();
      }

      echo "запрос выполнен успешно - таблица создана<br>";

      // Create a connection to MySQL
      $mysqli = new mysqli($host, $user, $password, $dbname);

      // Check if the connection was successful
      echo "connecting<br>";
      if ($mysqli->connect_errno) {
          die("Failed to connect to MySQL: " . $mysqli->connect_error);
      }

      echo "Select the database<br>";
      if (!$mysqli->select_db($dbname)) {
          die("Could not select database: " . $mysqli->error);
      }

      echo "Perform a query<br>";
      $result = $mysqli->query("SELECT * FROM SGroup");

      echo "Check if the query was successful<br>";
      if (!$result) {
          die("Query failed: " . $mysqli->error);
      }

      echo "Process the result<br>";
      while ($row = $result->fetch_assoc()) {
          echo $row['Num'] . ' ' . $row['Rating'] . '<br>';
      }

      echo "Export the database to a file<br>";
      system("mysqldump -u $user -p $password $dbname > backup.sql");

      // Close the connection
      $mysqli->close();
    ?>
  </body>
</html>

