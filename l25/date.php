<?php
$date = "undefined";
if(isset($_POST["date"])) {
    $date = $_POST["date"];
}
echo "Date: $date<br>";

$ok = true;

$dmyhms = explode(" ", $date);
$dmy = explode("-", $dmyhms[0]);
$hms = explode(":", $dmyhms[1]);
// check date and time given
if (count($dmyhms) != 2) {
    $ok = false;
}
// date checks
if (count($dmy) != 3) {
    $ok = false;
}
// day
if ((strlen($dmy[0]) != 2) | (intval($dmy[0]) > 31) | (intval($dmy[0]) <= 0)) {
    $ok = false;
}
// mounth
if ((strlen($dmy[1]) != 2) | (intval($dmy[1]) > 12) | (intval($dmy[1]) <= 0)) {
    $ok = false;
}
// year
if (!is_int(intval($dmy[2]))) {
    $ok = false;
}

// time checks
if (count($hms) != 3) {
    $ok = false;
}
// hour
if ((strlen($hms[0]) != 2) | (intval($hms[0]) > 24)) {
    $ok = false;
}
// min
if ((strlen($hms[1]) != 2) | (intval($hms[1]) > 60)) {
    $ok = false;
}
// sec
if ((strlen($hms[2]) != 2) | (intval($hms[2]) > 60)) {
    $ok = false;
}

if ($ok) {
    echo implode(".", $dmy);
    echo " ";
    echo implode(":", $hms);
    echo "<br>";
} else {
    echo "not correct";
}

?>
